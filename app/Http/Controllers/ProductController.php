<?php namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductController extends Controller
{
  public function index()
  {
      
      $result=DB::table('products')->paginate(2);
	return view('product.index')->with('data',$result);
  } 
  public function form()
  {
      return view('product.form');
  }
  
  public function delete($id)
  {
       $i=DB::table('products')->where('id',$id)->delete();
       if($i){
          \Session::flash('message','Record Have been Delete');
          return redirect('productindex');
       }
           
  }

  public function save1(Request $request)
  {
      $post=$request->all();
      $v   = \Validator::make($request->all(),
              [
                  'product_name' => 'required',
                  'product_price' => 'required',
                  'product_qty' => 'required',
              ]);
      if($v->fails())
      {
            return redirect()->back()->withErrors($v->errors());
      }
      else{
      $data = array(
          'product_name' => $post['product_name'],
          'product_price' => $post['product_price'],
          'product_qty' => $post['product_qty'],
          
      );
              $i=DB::table('products')->insert($data);
      if($i>0)
      {
          \Session::flash('message','Record Have been Saved');
          return redirect('productindex');
      }
      }
  }
  
  
  
}
