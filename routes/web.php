<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('productindex','ProductController@index'); 
Route::get('productform','ProductController@form');
Route::post('save1','ProductController@save1');
Route::get('DeleteProduct/{id}','ProductController@delete');
Auth::routes();

Route::get('/home', 'HomeController@index');
