-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2016 at 03:25 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ngit_admindb`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `product_qty` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_price`, `product_qty`) VALUES
(1, 'Keyboard', '1500', '20'),
(2, 'LED Screen', '10000', '20'),
(3, 'Mouse', '250', '20'),
(4, 'Switch', '200', '20'),
(5, 'avc', '200', '20'),
(6, 'avc', '200', '20'),
(7, 'avc', '200', '20'),
(8, 'avc', '200', '20'),
(9, 'avc', '200', '20'),
(10, 'avc', '200', '20'),
(11, 'avc', '200', '20'),
(12, 'avc', '200', '20'),
(13, 'avc', '200', '20'),
(14, 'avc', '200', '20'),
(15, 'avc', '200', '20'),
(16, 'avc', '200', '20'),
(17, 'avc', '200', '20'),
(18, 'avc', '200', '20'),
(19, 'avc', '200', '20'),
(20, 'avc', '200', '20'),
(21, 'avc', '200', '20'),
(22, 'avc', '200', '20'),
(23, 'avc', '200', '20'),
(24, 'avc', '200', '20'),
(25, 'avc', '200', '20'),
(26, 'avc', '200', '20'),
(27, 'avc', '200', '20'),
(28, 'avc', '200', '20'),
(29, 'avc', '200', '20'),
(30, 'avc', '200', '20'),
(31, 'avc', '200', '20'),
(32, 'avc', '200', '20'),
(33, 'avc', '200', '20'),
(34, 'avc', '200', '20'),
(35, 'avc', '200', '20'),
(36, 'avc', '200', '20'),
(37, 'avc', '200', '20'),
(38, 'avc', '200', '20'),
(39, 'avc', '200', '20'),
(40, 'avc', '200', '20'),
(41, 'avc', '200', '20'),
(42, 'avc', '200', '20'),
(43, 'avc', '200', '20'),
(44, 'avc', '200', '20'),
(45, 'avc', '200', '20'),
(46, 'avc', '200', '20'),
(47, 'avc', '200', '20'),
(48, 'avc', '200', '20'),
(49, 'avc', '200', '20'),
(50, 'avc', '200', '20'),
(51, 'avc', '200', '20'),
(52, 'avc', '200', '20'),
(53, 'avc', '200', '20'),
(54, 'avc', '200', '20'),
(55, 'avc', '200', '20'),
(56, 'avc', '200', '20'),
(57, 'avc', '200', '20'),
(58, 'avc', '200', '20'),
(59, 'avc', '200', '20'),
(60, 'avc', '200', '20'),
(61, 'avc', '200', '20'),
(62, 'avc', '200', '20'),
(63, 'avc', '200', '20'),
(64, 'avc', '200', '20'),
(65, 'avc', '200', '20'),
(66, 'avc', '200', '20'),
(67, 'avc', '200', '20'),
(68, 'czxc', '11', '2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nigam', 'nigam.shah@neogeninfotech.com', '$2y$10$KyfUmM11TBEVX9aciYdU5e5LDnRRpxxBMqzUX4t1zRVLoZ4QzNykO', 'BaplADzAjx7SsVxeIszZCSReBW24OFSjKvUEh4zjvatikRT82ZTLGgwTDNIg', '2016-10-21 01:54:22', '2016-10-21 05:43:27'),
(2, 'Chintan Shah', 'abc@gmail.com', '$2y$10$PjtQ5gVr5cqmoKZj5CUXv.JRVyBfjfJbtp1JLf3OuJEXwhJQqwUS2', '6FjtjJPHUzqxTLIAs98MGkKDVk3UDMOtEHX5nKhaOnJliNs40uV0zgCguyH4', '2016-10-21 05:44:01', '2016-10-21 05:44:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
