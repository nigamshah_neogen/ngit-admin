@extends('layout.master')
@section('content')
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
 
</head>
<p style="color:red">{{$errors->first('product_name')}}</p>
<p style="color:red">{{$errors->first('product_price')}}</p>
<p style="color:red">{{$errors->first('product_qty')}}</p>
<body>


    <form action="{{action('ProductController@save1')}}" method="post">
         <input name="_token" type="hidden" value="{{ csrf_token() }}">
       
        <input type="text" class="form-control" name="product_name" placeholder="Product Name">
       
    
          <input type="text" class="form-control" name="product_price" placeholder="Price">
      
     
          <input type="text" class="form-control" name="product_qty" placeholder="Qty">
        
          <br>
          <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
     
    </form>

   
    
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('assets/plugins/iCheck/icheck.min.js')}}"></script>

</body>
</html>
@stop()
